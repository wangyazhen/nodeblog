//var db = require('./db');

// var userCollection = require('./db');
var MongoClient = require('mongodb').MongoClient;

function getUsersCollection() {

}

function User (user) {
	if (!user){
		return;
	}
	this.name = user.name;
	this.password = user.password;
}

User.prototype.save = function(callback) {
	var user = {
		name: this.name,
		password: this.password
	};

	MongoClient.connect("mongodb://localhost:27017/testdb", function(err, db) {
	  if(err) {
	  	throw err;
	  }
	  console.log("We are connected");
	  var con =  db.collection('users');

	  con.insert(user, function(err, res) {	    
	    callback(err, res);
	  });
	});	
};

User.prototype.get = function(username, callback) {
	MongoClient.connect("mongodb://localhost:27017/testdb", function(err, db) {
	  if(err) {
	  	throw err;
	  }
	  console.log("We are connected");
	  
	  db.collection('users').findOne({name: username}, function(err, user) {
		callback(err, user);
	  });
	});
};

User.prototype.getAll = function(callback) {
	// var usersCollection = getUsersCollection();
	
	MongoClient.connect("mongodb://localhost:27017/testdb", function(err, db) {
	  if(err) {
	  	throw err;
	  }
	  console.log("We are connected");
	  
	  db.collection('users').find().toArray(function(err, datas) {
		callback(err, datas);
	  });		
	});
	
};
module.exports = User;