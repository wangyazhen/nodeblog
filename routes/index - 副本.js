var express = require('express');
var router = express.Router();

var User = require('../models/user');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/now', function(req, res) {
	res.send('The time: ', new Date().toString());
});
/*
var user = {
	'wangyz': {
		name: 'wangyz',
		age: 20,
		job: 'software engineer'		
	}
};
router.all('/user/:username', function(req, res, next) {
	console.log('all method ');
	if (user[req.params.username]) {
		next();
	} else {
		next(new Error(req.params.username +'  does not exist' ));
	}
});

router.get('/user/:username', function(req, res) {	
	res.send(JSON.stringify(user[req.params.username]));
});
*/
// 片段视图
router.get('/list', function(req, res) {
	res.render('list', {
		title: 'List',
		items: [2014, 'wangyzhen', 'node dev']
	});
});
// 建造微博网站  路由设计

// 用户的主页
router.get('/u/:user', function(req, res){

});
// 发表消息
router.post('/post', function(req, res) {

});
// 注册
router.get('/reg', function(req, res) {
	res.render('reg', {title: '用户注册'});
});
router.post('/reg', function(req, res) {
	var user = {name: req.body.username, password: req.body.password};
	console.log('我是注册 post', user);

	if (user.name && user.password) {
		new User(user).save(function(err, u) {
			
			// res.send(JSON.stringify(u));
			if (err) {
				throw err;
			}
			console.log('响应 ', u);
			var uname = u[0].name;
			res.render('index' , {title: uname});
		});		
	}
});
// 登陆
router.get('/login', function(req, res) {
	res.render('login', {title: '登陆', layout: 'layout'});
});
router.post('/login', function(req, res) {
	var username = req.body.username;
	var userpassword = req.body.password;
	new User().get(username, function(err, user) {
		if(user && user.password == userpassword) {
			res.send('登陆成功 '+ user.name);
		}else {
			res.send('用户：'+ username+ ' 还未注册！');
		}
	});
});
// 登出
router.get('/logout', function(req, res) {

});

router.get('/userList', function(req, res) {
	new User().getAll(function(e, userList) {
		if(e){
			throw e;
		}else {
			res.send(JSON.stringify(userList));
			//res.send('ok');
			console.log(userList);
		}
	});
});

module.exports = router;
