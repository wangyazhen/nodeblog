var express = require('express');
var router = express.Router();
var passport = require('passport');
var GithubStrategy = require('passport-github').Strategy;



/*
 * Github OAuth2.0认证
 * Github 首页 ：https://github.com/
 * 步骤如下
 * 	1：引导用户到github登录页面
 * 			http://localhost:3000/auth/github
 *  2：用户使用github账号登录，如果已经登录，则调过改步骤
 *  3：用户返回到系统
 * 			http://localhost:3000/auth/github/callback
 *  4：callback请求判断验证结果
 * 			成功 → http://localhost:3000/auth/github/index
 * 			失败 → http://localhost:3000/
 */

 passport.use('github', new GithubStrategy({
	clientID: "a5d5e75739390011860f",
	clientSecret: "1282db253e6e580a72deb450f35826e4f750bae4",
	callbackURL: "http://localhost:3000/auth/github/callback"
}, function(accessToken, refreshToken, profile, done) {
	done(null, profile);
}));

router.get("/github", passport.authenticate("github", {
	scope: "email"
}));

router.get("/github/callback",
	passport.authenticate("github", {
		successRedirect: '/auth/github/success',
		failureRedirect: '/auth/github/failure'
	}));

router.get('/github/success', function(req, res) {
	res.send('登陆成功');
});
router.get('/github/failure', function(req, res) {
	res.send('登陆  失败了');
});

passport.serializeUser(function(user, done) {
	console.log('serializeUser', user.username);
	done(null, user); //可以通过数据库方式操作
});

passport.deserializeUser(function(user, done) {
	console.log('deserializeUser');
	done(null, user); //可以通过数据库方式操作
});

module.exports = router;