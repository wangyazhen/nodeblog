var express = require('express');
var router = express.Router();

var UserService = require('../services/UserService');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' , layout: 'layout'});
});



// 用户的主页
router.get('/u/:user', function(req, res){

});
// 发表消息
router.post('/post', function(req, res) {

});
// 注册
router.get('/reg', function(req, res) {
	res.render('reg', {title: '用户注册'});
});
router.post('/reg', function(req, res) {
	var user = {name: req.body.username, password: req.body.password};
	console.log('我是注册 post', user);

	if (user.name && user.password) {
		UserService.save(user, function(err, u) {
			
			// res.send(JSON.stringify(u));
			if (err) {
				throw err;
			}
			console.log('响应 ', u);
			res.status(200).send(u)
		});		
	}
});
// 登陆
router.get('/login', function(req, res) {
	res.render('login', {title: '登陆', layout: 'layout'});
});
router.post('/login', function(req, res) {
	var username = req.body.username;
	var userpassword = req.body.password;
	new User().get(username, function(err, user) {
		if(user && user.password == userpassword) {
			res.send('登陆成功 '+ user.name);
		}else {
			res.send('用户：'+ username+ ' 还未注册！');
		}
	});
});
// 登出
router.get('/logout', function(req, res) {

});

router.get('/userList', function(req, res) {
	UserService.findAll(function(err, userList) {
		if (err) {
			console.error('获取用户列表出错:', err);
		} else {
			res.send(JSON.stringify(userList));
			console.log(userList);
		}
	});	
});

module.exports = router;
