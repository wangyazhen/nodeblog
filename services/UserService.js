var dbcon = require('../models/db');
var UserModel = require('../models/user');
var _ = require('underscore');

function User(){}

User.prototype.save = function(userobj, callback) {
	if(_.isEmpty(userobj)) { 
		callback(new Error('对象不能为空'));
	}	
	new UserModel(userobj).save(callback);
};

User.prototype.findAll = function(callback) {
	UserModel.find(callback);
};

User.prototype.delete = function(userId, callback) {
	UserModel.findByIdAndRemove(userId, function(err, user) {
		if (!user) {
			callback(new Error('对象 userid: '+ userId +'没有找到'));
		} else {
			callback(err, useer);
		}
	});
}

module.exports = new User();